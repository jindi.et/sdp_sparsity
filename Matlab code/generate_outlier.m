%%%%%%%%%%%%%%%%%%%%%%%%
%%% TO FILL
%%%%%%%%%%%%%%%%%%%%%%%%

function [dd_outlier, mu_NLOS, varargout] = generate_outlier(dd, n_fix, outlier_ratio, outlier_para, noisetype)

dd_tril = tril(dd);
dd_outlier = zeros(size(dd_tril));
Connect_Idx = find(dd_tril);
[Outlier_Idx] = randsample(Connect_Idx, round(outlier_ratio*length(Connect_Idx)));


N_outlier = length(Outlier_Idx);

switch noisetype
    case 'interval'
        outlier_low = outlier_para(1);
        outlier_up = outlier_para(2);
        noise_Vec = (outlier_up-outlier_low)*rand(N_outlier, 1) + outlier_low;   
    case 'additive normal'
        %         noise(Connect_dd_tril_Idx) = noise_Vec;
        noise_Vec = outlier_para*randn(N_Outlier, 1);
    case 'additive Laplacian'
        UnifRnd = rand(N_Outlier, 1) - 0.5;
        noise_Vec = outlier_para*sign(UnifRnd).*log(1-2*abs(UnifRnd));
        noise_Vec = outlier_para*randn(N_Outlier, 1);
end
dd_outlier(Outlier_Idx) = noise_Vec;

dd_outlier = tril(dd_outlier) + tril(dd_outlier)';

% find the true NLOS bias variable

dd_ss = dd(1:n_fix, 1:n_fix);
dd_ss_tril = tril(dd_ss);
dd_as = dd(n_fix+1:end, 1:n_fix);
N_link_ss = nnz(dd_ss_tril);  % #(connection between agents), the connections i-j and j-i are counted only once
N_link_as = nnz(dd_as);  % #(connection between agent and anchor)
[ii_ss, jj_ss] = find(dd_ss_tril);
[kk_as, ll_as] = find(dd_as);

ij_ss = sub2ind(size(dd_ss_tril), ii_ss, jj_ss);
kl_as = sub2ind(size(dd_as), kk_as, ll_as);

dd_outlier_ss = dd_outlier(1:n_fix, 1:n_fix);
mu_NLOS_ss = dd_outlier_ss(ij_ss);
dd_outlier_as = dd_outlier(n_fix + 1:end, 1:n_fix);
mu_NLOS_as = dd_outlier_as(kl_as);
mu_NLOS = [mu_NLOS_ss; mu_NLOS_as];

NLOS_Idx = Outlier_Idx;
LOS_Idx = setdiff(Connect_Idx, NLOS_Idx);

varargout{1} = LOS_Idx;
varargout{2} = NLOS_Idx;
end