%% !!!!!!This file is based on the sedumi solver. Please first download the solver. 
clear all;
clear K;

%% !!!!!!change this to the path of your sedumi solver 
addpath '/Users/dijin/Documents/1-MATLAB_part/Online_sources/sedumi-master'

%% Testing all methods for different P_NLOS
outlier_ratio = [0.1];  %   percentage of NLOS measurement 
lambda = 0.05;   %  regularization parameter of the sparsity-promoting regularized SDP
sigma_e_factor = 2; %   parameter c of the data-driven strategy

n_scale = 1;
nf_noise = 0.01; %  standard deviation of measurement error
nf_outlier = [0, 0.5]*n_scale;   %  two boundary values of the NLOS error's uniform distribution


%% network layout
% parameters for random sensor network with 100 agents
anchor = [0.1, 0.1; 0.1, 0.5; 0.1, 0.9; 0.5, 0.1; 0.5, 0.9; 0.9, 0.1; 0.9, 0.5; 0.9, 0.9].*n_scale;
m = length(anchor);
n_fix = 100;    % #(agents)
CommRange = 0.2*n_scale;



%% sensor network
[PP, dd, Connect_Mat] = Generate_network(anchor, n_fix, CommRange, n_scale);
agent = PP(:, 1:n_fix)';

%         noise_type = 'multiplicative';
noise_type = 'additive normal';
%                     noise_type = 'half normal';


%% range measurement with outlier
dd_error = generate_noise(dd, nf_noise, noise_type);
outlier_type = 'interval';
[dist_outlier, mu_NLOS] = generate_outlier(dd, n_fix, outlier_ratio, nf_outlier, outlier_type);
dd_outlier = dd_error + dist_outlier;

%% temperary
%         figure; histogram(dd_error(find(dd_error))-dd(find(dd))); hold on; histogram(dist_outlier(find(dist_outlier)));
%         histogram(dd(find(dd)))
%         legend('error', 'outlier', 'true distance')
%
%% sparsity-promoting regularized SDP
version_SDP = 'NLOS sparse SDP';
[X_sparse_SDP, Time_sparse_SDP_tmp, ~, x_opt_sparse_SDP] = SDP_SeDuMi(PP, m, dd_outlier, version_SDP, lambda);
plot_estimates(anchor, agent, X_sparse_SDP');

%% bound-constrained regularized SDP
version_SDP = 'NLOS min residual';
[X_min_residual, timing, ~, x_opt] = SDP_SeDuMi(PP, m, dd_outlier, version_SDP);
sigma_e_min = sum(x_opt.b_plus) + sum(x_opt.b_minus);

version_SDP = 'NLOS sparse SDP constraint MSE';
[X_MSE_min_sigma, Time_MSE_tmp] = SDP_SeDuMi(PP, m, dd_outlier, version_SDP, sigma_e_factor*sigma_e_min);
plot_estimates(anchor, agent, X_MSE_min_sigma');



