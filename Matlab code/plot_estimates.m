function plot_estimates(anchor, agent, agent_hat)
%% plot the position estimates of the SDP estimator

node = [agent;anchor];
n_fix = size(agent, 1);

figure;
scatter(anchor(:,1), anchor(:,2), 'bs', 'filled'); hold on
scatter(agent(:,1), agent(:,2), 'ro', 'filled');

scatter(agent_hat(:,1), agent_hat(:,2), 'g<', 'filled');
for i = 1:n_fix
    plot([node(i,1), agent_hat(i, 1)], [node(i,2), agent_hat(i, 2)], '-');
end

legend('anchor', 'agent', 'estimates');
grid on;