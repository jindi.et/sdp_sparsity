%%*************************************************************************
%%        Z_SeDuMi = [I_2, X_SeDuMi; X_SeDuMi^T, X^T*X];
%%
%% Input: PP = [agent positions, anchor positions]
%%         m = #(anchors)
%%        dd = [unknown point-unknown point distances,
%%              anchor-unknown point distances]
%%
%%
%% Output: X_SeDuMi:
%%         Z_SeDuMi:
%%
%%*************************************************************************


function [X_SeDuMi, timing, varargout] = SDP_SeDuMi(PP, m, dd, version, varargin)
n = size(PP, 2);
n_fix = n-m;
dd_ss = dd(1:n_fix, 1:n_fix);
dd_ss_tril = tril(dd_ss);
dd_as = dd(n_fix+1:end, 1:n_fix);
N_link_ss = nnz(dd_ss_tril);  % #(connection between agents), the connections i-j and j-i are counted only once
N_link_as = nnz(dd_as);  % #(connection between agent and anchor)
% pars.eps = 10^(-15);
pars.fid = 1;  % SeDuMi runs quitely
     
switch version
        
    case 'NLOS sparse SDP'
        lambda = varargin{1};
        [c_CostFunc, At, b, K, N_link_ss, N_link_as] = generateSeDuMi_NLOS_sparse_SDP(PP, m, dd, lambda);
        [x,y,info] = sedumi(At, b, c_CostFunc, K, pars);

        
        Z_SeDuMi = mat(x(end-(2+n_fix)^2+1:end), (2+n_fix));
        X_SeDuMi = Z_SeDuMi(1:2, 3:end);
        c = x(1:N_link_ss + N_link_as);
        b_plus_ij = x(N_link_ss + N_link_as + [1:N_link_ss]);  % b_plus_ij is the same as t^+_ij
        b_minus_ij = x(2*N_link_ss + N_link_as + [1:N_link_ss]);
        b_plus_kl = x(3*N_link_ss + N_link_as + [1:N_link_as]);
        b_minus_kl = x(3*N_link_ss + 2*N_link_as + [1:N_link_as]);
        
        %% ----------------------------------------------------
        % For Debug
        lambda_c_ij = ones(N_link_ss, 1) - At(:, 1:N_link_ss)'*y;
        lambda_c_kl = ones(N_link_as, 1) - At(:, N_link_ss + [1:N_link_as])'*y;
        lambda_b_ij_plus = 1/lambda.*ones(N_link_ss, 1) - At(:, N_link_ss + N_link_as + [1:N_link_ss])'*y;
        lambda_b_ij_minus = 1/lambda.*ones(N_link_ss, 1) - At(:, 2*N_link_ss + N_link_as + [1:N_link_ss])'*y;
        lambda_b_kl_plus = 1/lambda.*ones(N_link_as, 1) - At(:, 3*N_link_ss + N_link_as + [1:N_link_as])'*y;
        lambda_b_kl_minus = 1/lambda.*ones(N_link_as, 1) - At(:, 3*N_link_ss + 2*N_link_as + [1:N_link_as])'*y;
        lambda_Z = -At(:, 3*(N_link_ss + N_link_as)+1:end)'*y;
        %% ----------------------------------------------------
        x_opt.c = c;
        x_opt.b_plus = [b_plus_ij; b_plus_kl];
        x_opt.b_minus = [b_minus_ij; b_minus_kl];
        x_opt.Z = Z_SeDuMi;
        
        KKT_Ineq.c = [lambda_c_ij;lambda_c_kl];
        KKT_Ineq.b_plus = [lambda_b_ij_plus; lambda_b_kl_plus];
        KKT_Ineq.b_minus = [lambda_b_ij_minus; lambda_b_kl_minus];
        KKT_Ineq.Z = lambda_Z;
        
        varargout{1} = info.pinf;
        varargout{2} = x_opt;
        varargout{3} = KKT_Ineq;
        varargout{4} = y;
        timing = info.timing;
    
    case 'NLOS sparse SDP constraint MSE'
        %% variables
        %                  c_ij                     N_link_ss  non-negative parameter
        %                  c_kl                    N_link_as  non-negative parameter
        %                  b^+_ij                     N_link_ss  non-negative parameter
        %                  b^-_ij                    N_link_ss  non-negative parameter
        %                  b^+_kl                     N_link_as  non-negative parameter
        %                  b^-_kl                    N_link_as  non-negative parameter
        %                  sigma                      one non-negative parameter
        %                  Z = [I_2, X; X', Y] semi-definite cone of 2+ n_fix dimension

        
        sigma_e = varargin{1};
        [c_costFunc, At, b, K, N_link_ss, N_link_as] = generateSeDuMi_NLOS_sparse_SDP_constraint_MSE(PP, m, dd, sigma_e);
        [x,y,info] = sedumi(At, b, c_costFunc, K, pars);
        Z_SeDuMi = mat(x(end-(2+n_fix)^2+1:end), (2+n_fix));
        X_SeDuMi = Z_SeDuMi(1:2, 3:end);
        
        %% ----------------------------------------------------
        % For Debug
        sigma = x(K.l-1);
        c_ij = x(1:N_link_ss);
        c_kl= x(N_link_ss + [1:N_link_as]);
        b_ij_plus = x(N_link_ss + N_link_as + [1:N_link_ss]);
        b_ij_minus = x(2*N_link_ss + N_link_as + [1:N_link_ss]);
        
        b_kl_plus = x(3*N_link_ss + N_link_as + [1:N_link_as]);
        b_kl_minus = x(3*N_link_ss + 2*N_link_as + [1:N_link_as]);
        
        sigma = x(3*(N_link_ss + N_link_as) + 1);
        %% A = [A_up
%         %       A_low_c_ij, A_low_c_kl, A_low_b_plus_ij, A_low_]
%         A_c_ij = [zeros(3,N_link_ss); eye(N_link_ss); zeros(N_link_as, N_link_ss), zeros(1, N_link_ss)];
%         A_c_kl = [zeros(3,N_link_as); zeros(N_link_ss, N_link_as); eye(N_link_as), zeros(1, N_link_as)];
%         A_b_ij_plus = [zeros(3,N_link_ss); eye(N_link_ss); zeros(N_link_as, N_link_ss), ones(1, N_link_ss)];
%         A_b_ij_minus = [zeros(3,N_link_ss); -eye(N_link_ss); zeros(N_link_as, N_link_ss), ones(1, N_link_ss)];

        lambda_c_ij = ones(N_link_ss, 1) - At(:,1:N_link_ss)'*y;
        lambda_c_kl = ones(N_link_as, 1) - At(:, N_link_ss + [1:N_link_as])'*y;
        lambda_b_ij_plus = - At(:, N_link_ss + N_link_as + [1:N_link_ss])'*y; 
        lambda_b_ij_minus = - At(:, 2*N_link_ss + N_link_as + [1:N_link_ss])'*y; 
        lambda_b_kl_plus = - At(:, 3*N_link_ss + N_link_as + [1:N_link_as])'*y;  
        lambda_b_kl_minus = - At(:, 3*N_link_ss + 2*N_link_as + [1:N_link_as])'*y; 
        lambda_sigma = -At(:, 3*(N_link_ss + N_link_as)+ 1)'*y;
        lambda_Z = -At(:, 3*(N_link_ss + N_link_as)+2:end)'*y;
        
        KKT_Ineq.c = [lambda_c_ij;lambda_c_kl];
        KKT_Ineq.b_plus = [lambda_b_ij_plus; lambda_b_kl_plus];
        KKT_Ineq.b_minus = [lambda_b_ij_minus; lambda_b_kl_minus];
        KKT_Ineq.sigma = lambda_sigma;
        KKT_Ineq.Z = lambda_Z;
        
        x_opt.c = [c_ij; c_kl];
        x_opt.b_plus = [b_ij_plus; b_kl_plus];
        x_opt.b_minus = [b_ij_minus; b_kl_minus];
        x_opt.Z = Z_SeDuMi;
        x_opt.sigma = sigma;
        
        %% ----------------------------------------------------
        
        varargout{1} = info.pinf;
        varargout{2} = x_opt;
        varargout{3} = KKT_Ineq;  % if sigma = 0, constraint is active: sum_{(ij)} (b^+_ij + b^-_ij) + sum_{(kl)} (b^+_kl + b^-_kl) = \sigma_e
        %               and lambda_sigma_tilde > 0;
        % if sigma > 0; constraint is inactive: sum_{(ij)} (b^+_ij + b^-_ij) + sum_{(kl)} (b^+_kl + b^-_kl) > \sigma_e
        %               and lambda_sigma_tilde = 0;
        varargout{4} = [y(1:end-4);y(end-2:end)];  % excluding the KKT multiplier corresponding to the constraint: sigma_e - sum_{(ij)} (b^+_ij + b^-_ij) + sum_{(kl)} (b^+_kl + b^-_kl) = sigma 
        timing = info.timing;
        
                
    case 'NLOS min residual'
        [c_costFunc, At, b, K] = generateSeDuMi_NLOS_min_residual(PP, m, dd);
        [x,y,info] = sedumi(At, b, c_costFunc, K, pars);
        Z_SeDuMi = mat(x(end-(2+n_fix)^2+1:end), (2+n_fix));
        X_SeDuMi = Z_SeDuMi(1:2, 3:end);
        timing = info.timing;
        varargout{1} = info.pinf;
        
        c_ij = x(1:N_link_ss);
        c_kl= x(N_link_ss + [1:N_link_as]);
        b_ij_plus = x(N_link_ss + N_link_as + [1:N_link_ss]);
        b_ij_minus = x(2*N_link_ss + N_link_as + [1:N_link_ss]);
        b_kl_plus = x(3*N_link_ss + N_link_as + [1:N_link_as]);
        b_kl_minus = x(3*N_link_ss + 2*N_link_as + [1:N_link_as]);
        
        x_opt.c = [c_ij; c_kl];
        x_opt.b_plus = [b_ij_plus; b_kl_plus];
        x_opt.b_minus = [b_ij_minus; b_kl_minus];
        x_opt.Z = Z_SeDuMi;
        varargout{2} = x_opt;     
        
end



