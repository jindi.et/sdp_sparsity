function [PP, dd, Connect_Mat] = Generate_network(anchor, n_fix, CommRange, n_scale)


m = length(anchor);
Connect_Mat = sparse(n_fix);

while any( sum(Connect_Mat~=0)>2 == 0)
    
    agent = n_scale.*rand(n_fix, 2);
    PP = [agent; anchor]';
    
    %% dd <-- the matrix of true distance
    
    Dist_Mat = sparse(n_fix+m, n_fix+m);
    for i = 1:n_fix+m
        Diff = repmat(PP(:, i), 1, n_fix+m) - PP(:, :);
        Dist_i2All = sqrt(diag(Diff'*Diff));
        Dist_Mat(i, :) = Dist_i2All;
    end
    
    Connect_Mat = (Dist_Mat<=CommRange);
    Connect_Mat = Connect_Mat - eye(m+n_fix);
end
dd = Dist_Mat.*Connect_Mat;