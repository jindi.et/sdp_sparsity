function   [row, col] = tril2full(N, i)

A = zeros(N);

B = ones(N);
A = A + triu(B);

[row_tril, col_tril] = find(~A);

row = row_tril(i);
col = col_tril(i);