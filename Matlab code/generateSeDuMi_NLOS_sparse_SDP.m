%%*************************************************************************
%%%  
%%%  The original problem formulation is given as 
%%%  arg min sum_{ (i,j) } (1/lambda)*|r_ij -|x_i - x_j|_2 - c_ij|^2 +sum_{(i,j)} c_ij
%%%                     c_ij = mu_ij^2 + mu_ij*|x_i - x_j|  (this relation is ignored and we just treat c_ij as a new non-negative variable)
%%%
%%%  This problem can be relaxed to a semi-definite programming, given
%%%  as below
%%%                     arg min           (1/lambda)* (sum t^+_ij  + sum t^-_ij + sum t^+_kl  + sum t^-_kl) + sum c_ij + sum c_kl
%%% 
%%%                 subjected to         t^+_ij - t^-_ij = r_ij^2 - (Y_ii + Y_jj + 2Y_ij) - c_ij 
%%%                                             t^+_kl - t^-_kl = r_kl^2 - (|a_k|^2 + Y_ll + 2*a_k'*x_l) - c_kl 
%%%                                             Z = [I_2, X; X', Y] 
%%%  
%%%  
%%%  variables: c_ij                     N_link_ss  non-negative parameter  
%%%                  c_kl                    N_link_as  non-negative parameter  
%%%                  t^+_ij                     N_link_ss  non-negative parameter  
%%%                  t^-_ij                    N_link_ss  non-negative parameter
%%%                  t^+_kl                     N_link_ss  non-negative parameter  
%%%                  t^-_kl                    N_link_ss  non-negative parameter
%%%                  Z = [I_2, X; X', Y] semi-definite cone of 2+ n_fix dimension
%%%  
%%%
%%% Input: PP = [agent positions, anchor positions]
%%%        m = #(anchors)
%%%        dd = [unknown point-unknown point distances, 
%%%              anchor-unknown point distances]
%%%        
%%%       
%%% Output: At: constraint matrix (dimension = #(constraints)*#(variables))
%%%         b: Constraint equation coefficients  (dimension = #(constraints)*1)
%%%         c: Objective function coefficient (dimension = #(variables)*1)
%%%         K: self-dual cone 
%%%
%%%*************************************************************************

function  [c, At, b, K, N_link_ss, N_link_as] = generateSeDuMi_NLOS_sparse_SDP(PP, m, dd, lambda)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = size(PP, 2);
n_fix = n - m;
anchor = PP(:, n_fix+1:end);

dd_ss = dd(1:n_fix, 1:n_fix);  
dd_ss_tril = tril(dd_ss);
dd_as = dd(n_fix+1:end, 1:n_fix);
N_link_ss = nnz(dd_ss_tril);  % #(connection between agents), the connections i-j and j-i are counted only once
N_link_as = nnz(dd_as);  % #(connection between agent and anchor)
I_ss = eye(n_fix);

[ii_ss, jj_ss, dist_ss] = find(dd_ss_tril);
[kk_as, ll_as, dist_as] = find(dd_as);

%% variables
%                  c_ij                     N_link_ss  non-negative parameter  
%                  c_kl                    N_link_as  non-negative parameter  
%                  t^+_ij                     N_link_ss  non-negative parameter  
%                  t^-_ij                    N_link_ss  non-negative parameter
%                  t^+_kl                     N_link_as  non-negative parameter  
%                  t^-_kl                    N_link_as  non-negative parameter
%                  Z = [I_2, X; X', Y] semi-definite cone of 2+ n_fix dimension   

K.l = 3*(N_link_ss+N_link_as);
K.s = 2 + n_fix;
N_var = K.l +(2+n_fix)^2;


%% target function 
c_mu = ones(1, N_link_ss + N_link_as);
c_t = 1./lambda.*ones(1, 2*(N_link_ss + N_link_as));
c_z = zeros(1, (2+n_fix)^2);

c = [c_mu, c_t, c_z];
c = sparse(c);


%% constraints
N_con = N_link_ss + N_link_as + 3;
At = zeros(N_con, N_var);
b = zeros(N_con, 1);

% 1.  for any agent-agent connection (i, j)
%   t^+_ij - t^-_ij = r_ij^2 - (Y_ii + Y_jj + 2Y_ij) - c_ij --> 
%   c_ij + t^+_ij -t^-_ij + tr(K_ij, Z) = r_ij^2

for i_con = 1: N_link_ss
    At(i_con, i_con) = 1; % c_ij
    At(i_con, N_link_ss + N_link_as + i_con) = 1; % t^+_ij
    At(i_con, N_link_ss*2 + N_link_as + i_con)  = -1; % -t^-_ij
    % tr(K_ij, Z)  with K_ij = [0; 0; ei-ej] * [0, 0, ei'-ej'] = [0; 0; ej-ei] * [0, 0, ej'-ei'] 
    vec_tmp = I_ss(:, jj_ss(i_con))-I_ss(:, ii_ss(i_con));   
    vec_sel = [0;0;vec_tmp];
    K_ij = vec_sel*vec_sel';
    At(i_con, 3*(N_link_ss + N_link_as) + [1:(2+n_fix)^2]) = K_ij(:);
    b(i_con) = dist_ss(i_con)^2;   % r_ij^2
end

% for any anchor-agent connection (k, l)
% t^+_kl - t^-_kl = r_kl^2 - (|a_k|^2 + Y_ll + 2*a_k'*x_l) - c_kl  -->
% c_kl + t^+_kl - t^-_kl + tr(K_kl, Z) = r_kl^2

for i_con = 1:N_link_as
    con_offset = N_link_ss;
    At(i_con + con_offset, N_link_ss + i_con) = 1; % c_kl
    At(i_con + con_offset, 3*N_link_ss + N_link_as + i_con) = 1;% t^+_kl
    At(i_con + con_offset, 3*N_link_ss + 2*N_link_as + i_con) = -1; % - t^-_kl
    
    % tr(K_kl, Z) with K_kl = [ak; -el]*[ak', -el']; 
    vec_tmp = - I_ss(:, ll_as(i_con));
    ak = PP(:, n_fix + kk_as(i_con));
    vec_sel = [ak; vec_tmp];
    K_kl = vec_sel*vec_sel';
    At(i_con + con_offset, K.l + [1:(2+n_fix)^2]) = K_kl(:);
    b(i_con + con_offset) = dist_as(i_con)^2;
end

% Z = [I_2, X; X', Y]
con_offset = N_link_ss + N_link_as;
At(con_offset + 1, K.l + 1) = 1;
b(con_offset + 1) = 1;

At(con_offset + 2, K.l + 2) = 1;
b(con_offset + 2) = 0;

At(con_offset + 3, K.l + 2+n_fix + 2) = 1;
b(con_offset + 3) = 1;

end