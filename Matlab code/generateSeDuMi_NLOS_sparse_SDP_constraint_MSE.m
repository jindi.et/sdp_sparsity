%%*************************************************************************
%%% Generate SOCP constraints and objective for SEDUMI corresponding to 
%%%  
%%%  The original problem formulation is given as 
%%%                 arg min            sum_{(i,j)} c_ij + sum_{(k,l)} c_kl
%%%             subjected to        c >= 0, b^+ >= 0, b^- >= 0, sigma >= 0, Z >= 0,
%%%                                         b^+_ij - b^-_ij = r_ij^2 - tr(A_ij *Z) -c_ij
%%%                                         b^+_kl - b^-_kl = r_kl^2 - tr(A_kl *Z) -c_kl
%%%                                         sum_{(ij)} (b^+_ij + b^-_ij) +
%%%                                         sigma + sum_{(kl)} (b^+_kl + b^-_kl) = sigma_e
%%%                                         Z = [I_2, X; X', Y]
%%%
%%%  variables: c_ij                         N_link_ss  non-negative parameter  
%%%                  c_kl                        N_link_as  non-negative parameter  
%%%                  b^+_ij                      N_link_ss  non-negative parameter  
%%%                  b^-_ij                       N_link_ss  non-negative parameter
%%%                  b^+_kl                     N_link_ss  non-negative parameter  
%%%                  b^-_kl                      N_link_ss  non-negative parameter
%%%                  sigma                      one non-negative parameter
%%%                  Z = [I_2, X; X', Y]    semi-definite cone of 2+ n_fix dimension
%%%  
%%%
%%% Input: PP = [agent positions, anchor positions]
%%%        m = #(anchors)
%%%        dd = [unknown point-unknown point distances, 
%%%              anchor-unknown point distances]
%%%        
%%%       
%%% Output: At: constraint matrix (dimension = #(constraints)*#(variables))
%%%         b: Constraint equation coefficients  (dimension = #(constraints)*1)
%%%         c: Objective function coefficient (dimension = #(variables)*1)
%%%         K: self-dual cone 
%%%
%%%*************************************************************************

function  [c, At, b, K, N_link_ss, N_link_as] = generateSeDuMi_NLOS_sparse_SDP_constraint_MSE(PP, m, dd, sigma_e)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = size(PP, 2);
n_fix = n - m;
anchor = PP(:, n_fix+1:end);

dd_ss = dd(1:n_fix, 1:n_fix);  
dd_ss_tril = tril(dd_ss);
dd_as = dd(n_fix+1:end, 1:n_fix);
N_link_ss = nnz(dd_ss_tril);  % #(connection between agents), the connections i-j and j-i are counted only once
N_link_as = nnz(dd_as);  % #(connection between agent and anchor)
I_ss = eye(n_fix);

[ii_ss, jj_ss, dist_ss] = find(dd_ss_tril);
[kk_as, ll_as, dist_as] = find(dd_as);

%% variables
%                  c_ij                     N_link_ss  non-negative parameter  
%                  c_kl                    N_link_as  non-negative parameter  
%                  b^+_ij                     N_link_ss  non-negative parameter  
%                  b^-_ij                    N_link_ss  non-negative parameter
%                  b^+_kl                     N_link_as  non-negative parameter  
%                  b^-_kl                    N_link_as  non-negative parameter
%                  sigma                      one non-negative parameter
%                  Z = [I_2, X; X', Y] semi-definite cone of 2+ n_fix dimension   

K.l = 3*(N_link_ss+N_link_as) + 1;
K.s = 2 + n_fix;
N_var = K.l +(2+n_fix)^2;


%% target function: sum_{(i,j)} c_ij + sum_{(k,l)} c_kl
c_mu = ones(1, N_link_ss + N_link_as);
c_t = zeros(1, 2*(N_link_ss + N_link_as)+1);
c_z = zeros(1, (2+n_fix)^2);

c = [c_mu, c_t, c_z];
c = sparse(c);

%% constraints
%                                         b^+_ij - b^-_ij = r_ij^2 - tr(A_ij *Z) -c_ij
%                                         b^+_kl - b^-_kl = r_kl^2 - tr(A_kl *Z) -c_kl
%                                         sum_{(ij)} (b^+_ij + b^-_ij) + sum_{(kl)} (b^+_kl + b^-_kl) + sigma = \sigma_e
%                                         (sum_{(ij)} (b^+_ij + b^-_ij) + sum_{(kl)} (b^+_kl + b^-_kl) >= \sigma_e)  
%                                         Z = [I_2, X; X', Y]
% 
N_con = N_link_ss + N_link_as + 1+3;
At = zeros(N_con, N_var);
b = zeros(N_con, 1);

% 1.  for any agent-agent connection (i, j)
% b^+_ij - b^-_ij = r_ij^2 - tr(A_ij *Z) -c_ij --> c_ij + b^+_ij - b^-_ij + tr(A_ij *Z) = r_ij^2 

for i_con = 1: N_link_ss
    At(i_con, i_con) = 1; % c_ij
    At(i_con, N_link_ss + N_link_as + i_con) = 1; % b^+_ij
    At(i_con, N_link_ss*2 + N_link_as + i_con)  = -1; % -b^-_ij
    % tr(A_ij, Z)  with A_ij = [0; 0; ei-ej] * [0, 0, ei'-ej'] = [0; 0; ej-ei] * [0, 0, ej'-ei'] 
    vec_tmp = I_ss(:, jj_ss(i_con))-I_ss(:, ii_ss(i_con));   
    vec_sel = [0;0;vec_tmp];
    A_ij = vec_sel*vec_sel';
    At(i_con, K.l + [1:(2+n_fix)^2]) = A_ij(:);  % tr(A_ij*Z)
    b(i_con) = dist_ss(i_con)^2;   % r_ij^2
end

% for any anchor-agent connection (k, l)
% b^+_kl - b^-_kl = r_kl^2 - tr(A_kl *Z) -c_kl --> c_kl + b^+_kl - b^-_kl + tr(A_kl *Z) = r_kl^2

for i_con = 1:N_link_as
    con_offset = N_link_ss;
    At(i_con + con_offset, N_link_ss + i_con) = 1; % c_kl
    At(i_con + con_offset, 3*N_link_ss + N_link_as + i_con) = 1;% b^+_kl
    At(i_con + con_offset, 3*N_link_ss + 2*N_link_as + i_con) = -1; % - b^-_kl
    
    % tr(A_kl, Z) with A_kl = [ak; -el]*[ak', -el']; 
    vec_tmp = - I_ss(:, ll_as(i_con));
    ak = PP(:, n_fix + kk_as(i_con));
    vec_sel = [ak; vec_tmp];
    A_kl = vec_sel*vec_sel';
    At(i_con + con_offset, K.l + [1:(2+n_fix)^2]) = A_kl(:); % % tr(A_ij*Z)
    b(i_con + con_offset) = dist_as(i_con)^2; % r_kl^2
end

% sigma_e - sum_{(ij)} (b^+_ij + b^-_ij) + sum_{(kl)} (b^+_kl + b^-_kl) = sigma -->  sigma + sum_{(ij)} (b^+_ij + b^-_ij) + sum_{(kl)} (b^+_kl + b^-_kl) = sigma_e
con_offset = N_link_ss + N_link_as;
var_offset = N_link_ss + N_link_as;

At(con_offset + 1, var_offset + [1:2*(N_link_ss + N_link_as)+1])  = ones(1, 2*(N_link_ss + N_link_as)+1); % b^+_ij, b^-_ij, b^+_kl, b^-_kl, sigma
b(con_offset + 1) = sigma_e;

% Z = [I_2, X; X', Y]
con_offset = N_link_ss + N_link_as + 1;
At(con_offset + 1, K.l + 1) = 1;
b(con_offset + 1) = 1;

At(con_offset + 2, K.l + 2) = 1;
b(con_offset + 2) = 0;

At(con_offset + 3, K.l + 2+n_fix + 2) = 1;
b(con_offset + 3) = 1;

end