%% !!!!!!This file is based on the sedumi solver. Please first download the solver. 
clear all;
clear K;

%% !!!!!!change this to the path of your sedumi solver 
addpath '/Users/dijin/Documents/1-MATLAB_part/Online_sources/sedumi-master'

%% Testing all methods for different P_NLOS
outlier_ratio_all = linspace(0, 0.5, 6);    %   percentage of NLOS measurement
lambda_all = [0.01, 0.05, 0.1]; %  regularization parameter of the sparsity-promoting regularized SDP
sigma_e_factor = [2];   %   parameter c of the data-driven strategy


N_MC = 100;
n_scale = 1;
nf_noise = 0.01;    %  standard deviation of measurement error
nf_outlier = [0, 0.5]*n_scale;  %  two boundary values of the NLOS error's uniform distribution

N_outlier_ratio = length(outlier_ratio_all);
N_lambda = length(lambda_all);
N_factor = length(sigma_e_factor);

% preallocation
RMSE_sparse_SDP = zeros(N_outlier_ratio, N_lambda); 
RMSE_MSE_min_sigma = zeros(N_outlier_ratio, N_factor);


for ii = 1:N_outlier_ratio
    outlier_ratio = outlier_ratio_all(ii)
    %% network layout
    % parameters for random sensor network with 100 agents
    anchor = [0.1, 0.1; 0.1, 0.5; 0.1, 0.9; 0.5, 0.1; 0.5, 0.9; 0.9, 0.1; 0.9, 0.5; 0.9, 0.9].*n_scale;
    m = length(anchor);
    n_fix = 100;
    CommRange = 0.2*n_scale;
    
    %% reset
    RMSE_sparse_SDP_tmp = zeros(N_lambda, N_MC);
    RMSE_MSE_min_sigma_tmp = zeros(N_factor, N_MC);
    
    for i_MC = 1:N_MC
        %% reset
        X_sparse_SDP = zeros(N_lambda, 2, n_fix);
        X_MSE_min_sigma = zeros(N_factor, 2, n_fix);
        
        %% sensor network
        [PP, dd, Connect_Mat] = Generate_network(anchor, n_fix, CommRange, n_scale);  
        agent = PP(:, 1:n_fix)';
        
%         noise_type = 'multiplicative';
        noise_type = 'additive normal';
%                     noise_type = 'half normal';
        
        
        %% range measurement with outlier
        dd_error = generate_noise(dd, nf_noise, noise_type);
        outlier_type = 'interval';
        [dist_outlier, mu_NLOS] = generate_outlier(dd, n_fix, outlier_ratio, nf_outlier, outlier_type);
        dd_outlier = dd_error + dist_outlier;
        
        %% temperary        
%         figure; histogram(dd_error(find(dd_error))-dd(find(dd))); hold on; histogram(dist_outlier(find(dist_outlier)));
%         histogram(dd(find(dd)))
%         legend('error', 'outlier', 'true distance')
%         
        %% sparsity-promoting regularized SDP
        for i_lambda = 1:N_lambda
            version_SDP = 'NLOS sparse SDP';
            [X_sparse_SDP(i_lambda, :, :), Time_sparse_SDP_tmp, ~, x_opt_sparse_SDP] = SDP_SeDuMi(PP, m, dd_outlier, version_SDP, lambda_all(i_lambda));
            Time_sparse_SDP_all(ii, i_lambda, i_MC) = sum(Time_sparse_SDP_tmp);
            RMSE_sparse_SDP_tmp(i_lambda, i_MC) = RMSE_calculator(X_sparse_SDP(i_lambda, :, :), agent');
%             sigma_e_equi(ii, i_MC, i_lambda) = sum(x_opt_sparse_SDP.b_plus) + sum(x_opt_sparse_SDP.b_minus);
            
        end
        
        %% bound-constrained regularized SDP        
        version_SDP = 'NLOS min residual';
        [X_min_residual, timing, ~, x_opt] = SDP_SeDuMi(PP, m, dd_outlier, version_SDP);
        sigma_e_min(ii, i_MC) = sum(x_opt.b_plus) + sum(x_opt.b_minus);
        
        version_SDP = 'NLOS sparse SDP constraint MSE';
        for i_factor = 1:N_factor
        [X_MSE_min_sigma(i_factor, :, :), Time_MSE_tmp] = SDP_SeDuMi(PP, m, dd_outlier, version_SDP, sigma_e_factor(i_factor)*sigma_e_min(ii, i_MC));
        Time_MSE_all(ii, i_factor, i_MC) = sum(timing) + sum(Time_MSE_tmp);
        RMSE_MSE_min_sigma_tmp(i_factor, i_MC) = RMSE_calculator(X_MSE_min_sigma(i_factor, :, :), agent');
        end
               
    end
    
    %% RMSE
    for i_lambda = 1:N_lambda
        RMSE_sparse_SDP(ii, i_lambda) = sqrt(mean(RMSE_sparse_SDP_tmp(i_lambda, :).^2));
    end
    
    for i_factor = 1:N_factor
        RMSE_MSE_min_sigma(ii, i_factor) = sqrt(mean(RMSE_MSE_min_sigma_tmp(i_factor, :).^2));
    end
    
end

